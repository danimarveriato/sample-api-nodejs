const express = require('express');
//Importação da biblioteca que permite que a API seja vista por outros domínios
const cors = require('cors');
const mongoose = require('mongoose');
//Importa o pacote que faz o require dos models automaticamente
const requireDir = require('require-dir'); 

//Iniciando o App
const app = express();
//Permite que seja enviado dados para a aplicação no formato JSON (mais indicado para API REST)
app.use(express.json());
//Habilitando o acesso de outros domínios na API
app.use(cors());

//Iniciando o DB  |protocolo|ip e usuario e senha - Se tiver|porta|nome schema
mongoose.connect('mongodb://localhost:27017/nodeapi', 
    { useNewUrlParser:true }
);

//Importa todos os models do diretorio
requireDir('./src/models');


//Rotas
app.use('/api', require("./src/routes"));


//Aplicação vai ouvir a porta 3001 do navegador
app.listen(3001);
const express = require("express");
const routes = express.Router();

const ProductController = require ("./controllers/ProductController");

//Rotas
//Quando vai buscar algo no BD, usa-se GET
routes.get("/products", ProductController.index);

//Busca um produto pelo ID
routes.get("/products/:id", ProductController.show);

//Sempre que for criar alguma coisa, usa-se o POST
routes.post("/products", ProductController.store);

//Atualiza um registro no BD
routes.put("/products/:id", ProductController.update);

//Apaga um registro no BD
routes.delete("/products/:id", ProductController.destroy);

module.exports = routes;
const mongoose = require('mongoose');

const Product = mongoose.model('Product');


module.exports = {
    //Criação dos métodos de requisição da API
    //Parâmetro req, significa a requisição que estamos fazendo ao servidor.
    //Parametro res, é a resposta da requisição feita ao servidor

    //Busca todos os produtos do BD
    async index(req, res){

        //Retorna todos os produtos
        //const products = await Product.find();

        //Permite ao usuário passar por parâmetro a página com os registros paginados
        const { page = 1 } = req.query;

        //Retorna os produtos paginados de 10 em 10
        const products = await Product.paginate({}, { page, limit: 10 });

        return res.json(products);
    },

    //Busca um produto pelo ID
    async show(req, res){
        const product = await Product.findById(req.params.id);

        return res.json(product);
    },

    //Faz o insert de um produto (VERIFICAR)
    async store (req, res){
        const product = await Product.create(req.body);

        return res.json(product);
    },

    //Faz o update de um registro no BD (VERIFICAR)
    async update(req, res){
        //New:true, retorna o produto atualizado pra dentro da variável product
        const product = await Product.findOneAndUpdate(req.params.id, req.body, { new: true }); 

        return res.json(product);
    },

    //Apaga um registro do BD
    async destroy(req, res){
        await Product.findByIdAndRemove(req.params.id);

        return res.send(); 
    }

};